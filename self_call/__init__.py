import random, json
from requests import get, post, utils

# Custom exception for authorization errors
class AuthError(ValueError):
 
    def __init__(self, value):
        self.value = value
 
    def __str__(self):
        return repr(self.value)
        
# Custom exception for stream errors
class StreamError(ValueError):
 
    def __init__(self, value):
        self.value = value
 
    def __str__(self):
        return repr(self.value)

# Function for making HTTP POST request to Bing
def bing_purgpt(model, messages, key):
    bing_models = {
        'bing-creative': 'Creative',
        'bing-balanced': 'Balanced',
        'bing-precise': 'Precise'
    }
    bing_url = ["https://purgpt.xyz/v1/bing", "?message=", str(messages[-1]['content']), "&key=", key, "&variant=", bing_models[model]]
    return post(url=''.join(bing_url))

# Function for making HTTP POST request to Nova app
def nova_app_call(model, messages, temperature, streaming):
    
    # Generate random user id
    user_id = ''.join(random.choices('0123456789abcdef', k=32))
    
    X_stream = str(streaming).lower()
    
    # Model selection
    model_mapping = {"gpt-4-0613": '2', "gpt-3.5-turbo-0613": '0'}
    X_model = model_mapping.get(model, '1')
    
    return post(
        url = 'https://api.novaapp.ai/api/chat',
        headers = {
            'X_stream': X_stream,
            'X_platform': 'android',
            'X_user_id': user_id,
            'X_pr': 'true',
            'X_dev': 'false',
            'X_model': X_model,
            'User-Agent': 'okhttp/5.0.0-alpha.11'
        },
        json = {
            'model' : 'gpt-3.5-turbo',
            'messages' : messages,
            'temperature' : temperature
        },
        stream = streaming
    )

# Function for making HTTP POST request to Hazi
def hazi_call(model, messages, temperature):
    return post(
        url = 'https://hazi-response.vercel.app/api/generate/chat',
        headers = {'User-Agent': 'Gola/25 CFNetwork/1408.0.4 Darwin/22.5.0'},
        json = {
            'model' : model,
            'messages' : messages,
            'temperature' : temperature
        },
        stream = False
    )

# Dispatch function to choose the provider from where response should be received
def get_response(model, messages, provider, temperature=0.7, streaming=True, special=None):
    if provider == "hazi":
        if streaming:
            raise StreamError("This provider does not support streaming!")
        else:
            response = hazi_call(model=model, messages=messages, temperature=temperature)
            return json.loads(response.text)["answer"]

    elif provider == "nova_app":
        response = nova_app_call(model=model, messages=messages, temperature=temperature, streaming=streaming)
        if not streaming:
            return json.loads(response.text)["choices"][0]["messages"]["content"]
        for token in response:
            if b'content' in event:
                json_response = json.loads(event.decode().split('data: ')[1])
                yield json_response["choices"][0]["delta"]["content"]
            else:
                yield ''
                
    elif provider == "bing":
        if special is None:
            raise AuthError("This provider requires a key.")
        response = bing_purgpt(model=model, messages=messages, key=special)
        return json.loads(response.text)["text"]