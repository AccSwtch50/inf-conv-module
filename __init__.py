# Import the required libraries
import sys, os
from . import self_call
import pkgutil

__all__ = ["get_response"] 

# Import g4f module
sys.path.append(os.path.join(os.path.dirname(__file__), 'gpt4free'))
import g4f

class AuthError(ValueError):
    """
    Custom exception class for handling authentication errors.
    """
    def __init__(self, value):
        self.value = value
 
    def __str__(self):
        return repr(self.value)

g4f_providers = [name for _, name, _ in pkgutil.iter_modules(g4f.Provider.__path__)]

auth_providers = ['purgpt_bing']

#g4f_auth_providers = [name for _, name, _ in pkgutil.iter_modules(g4f.Provider.need_auth.__path__)]

g4f_auth_providers_docstring = """
    Bard,
    Bing,
    HuggingChat,
    OpenAssistant,
    OpenaiChat,
"""

g4f_auth_providers = g4f_auth_providers_docstring.strip().split(",")

g4f_auth_providers = [string.strip() for string in g4f_auth_providers]

auth_providers.append(g4f_auth_providers)

provider_details = {
    "providers": {
        "self_call": ["hazi", "nova_app", "purgpt_bing"],
        "g4f": g4f_providers,
    },
    "non_streaming_providers": ["hazi"],
    "auth_providers": auth_providers,
    "provider_name_dict": {
        'purgpt_bing': 'bing'
    },
    "default_provider": {
        'gpt-4': 'nova_app'
    }
}

def get_response(model, messages, temperature, streaming, provider=None, special=None):
    """
    This function returns responses from different service providers.
    """
    if provider is None:
        provider = provider_details["default_provider"][model]

    if provider in provider_details["non_streaming_providers"]:
        streaming = False

    given_provider = provider_details["provider_name_dict"].get(provider, provider)

    auth = False

    if special is None:
        if provider in provider_details["auth_providers"]:
            raise AuthError("This provider requires a special thing (usually authentication).")
    else:
        auth = True

    if provider in provider_details["providers"]["self_call"]:
        return self_call.get_response(model=model, messages=messages, temperature=temperature, streaming=streaming, provider=given_provider, special=special)
    
    elif provider in provider_details["providers"]["g4f"]:
        return g4f.ChatCompletion.create(model=model, provider=getattr(g4f.Provider, given_provider), messages=messages, stream=streaming, cookies=special, auth=auth)
    
    else:
        raise ValueError(f"The provider \"{provider}\" is not recognized.")